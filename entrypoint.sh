#!/bin/sh

python /application/payment_prototype/manage.py migrate

python /application/payment_prototype/manage.py update_rates

python /application/payment_prototype/manage.py runserver 0.0.0.0:8000
