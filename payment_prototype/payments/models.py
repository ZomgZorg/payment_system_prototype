# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import uuid

from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from django.db import models, transaction
from djmoney.contrib.exchange.models import convert_money

from djmoney.models.fields import MoneyField
from djmoney.money import Money


class Account(models.Model):
    id = models.UUIDField(
        primary_key=True,
        default=uuid.uuid4,
        editable=False
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        verbose_name=_('User'),
        related_name='accounts',
        on_delete=models.PROTECT
    )
    balance = MoneyField(
        verbose_name=_('Balance'),
        max_digits=19,
        decimal_places=4,
        default=0,
    )
    created_at = models.DateTimeField(
        verbose_name=_('Created at'),
        auto_now_add=True
    )
    updated_at = models.DateTimeField(
        verbose_name=_('Updated at'),
        auto_now=True
    )
    is_active = models.BooleanField(
        verbose_name=_('Active'),
        default=True
    )

    class Meta:
        verbose_name = _('Account')
        verbose_name_plural = _('Accounts')
        ordering = ('created_at', )


class Transaction(models.Model):
    id = models.UUIDField(
        primary_key=True,
        default=uuid.uuid4,
        editable=False
    )
    source = models.ForeignKey(
        Account,
        verbose_name=_('Source account'),
        related_name='outcoming_transactions',
        null=True,
        on_delete=models.SET_NULL
    )
    target = models.ForeignKey(
        Account,
        verbose_name=_('Target account'),
        related_name='incoming_transactions',
        null=True,
        on_delete=models.SET_NULL
    )
    amount = MoneyField(
        verbose_name=_('Amount'),
        max_digits=19,
        decimal_places=4,
    )
    commission_amount = MoneyField(
        verbose_name=_('Commission amount'),
        max_digits=19,
        decimal_places=4,
    )
    comment = models.CharField(
        verbose_name=_('Comment'),
        max_length=255,
        blank=True,
    )
    created_at = models.DateTimeField(
        verbose_name=_('Created at'),
        auto_now_add=True,
    )

    class Meta:
        verbose_name = _('Transaction')
        verbose_name_plural = _('Transactions')
        ordering = ('created_at', )

    @staticmethod
    def calculate_commission_amount(amount):
        return amount * settings.COMMISSION_AMOUNT_PERCENT / 100

    @classmethod
    def create(cls, source_id, target_id, amount, comment='', commission_amount=None):
        with transaction.atomic():
            try:
                source = Account.objects.select_for_update().get(pk=source_id)
            except Account.DoesNotExist:
                raise ValidationError(_(f'Wrong source account {source_id} specified'))
            try:
                target = Account.objects.select_for_update().get(pk=target_id)
            except Account.DoesNotExist:
                raise ValidationError(_(f'Wrong target account {target_id} specified'))
            if target == source:
                raise ValidationError(_('Target cannot be the same as source'))

            if commission_amount is None:
                commission_amount = cls.calculate_commission_amount(amount) if source.user != target.user else 0
            amount = Money(amount, source.balance_currency)
            commission_amount = Money(commission_amount, source.balance_currency)
            if amount + commission_amount > source.balance:
                raise ValidationError(_('Not enough money on specified source account'))

            source.balance -= amount + commission_amount
            source.save()

            target.balance += convert_money(amount, target.balance_currency)
            target.save()

            instance = cls(
                source=source,
                target=target,
                amount=amount,
                commission_amount=commission_amount,
                comment=comment,
            )
            instance.save()

        return instance

    @classmethod
    def deposit(cls, target_id, amount, amount_currency=None, comment=''):
        with transaction.atomic():
            try:
                target = Account.objects.select_for_update().get(pk=target_id)
            except Account.DoesNotExist:
                raise ValidationError(_(f'Wrong target account {target_id} specified'))
            if not amount_currency:
                amount_currency = target.balance_currency
            amount = Money(amount, amount_currency)

            target.balance += convert_money(amount, target.balance_currency)
            target.save()

            instance = cls(
                source=None,
                target=target,
                amount=amount,
                commission_amount=Money(0, target.balance_currency),
                comment=comment,
            )
            instance.save()

        return instance

    def __str__(self):
        return str(self.id)
