import uuid

import pytest
from djmoney.money import Money

from payments.models import Account, Transaction


@pytest.fixture
def create_user(db, django_user_model):
    def make_user(**kwargs):
        kwargs['password'] = 'some_password'
        if 'username' not in kwargs:
            kwargs['username'] = str(uuid.uuid4().hex)[:30]
        return django_user_model.objects.create_user(**kwargs)
    return make_user


@pytest.fixture
def create_account(db, create_user):
    def make_account(**kwargs):
        if 'user' not in kwargs:
            kwargs['user'] = create_user()
        if 'balance' not in kwargs:
            kwargs['balance'] = Money(0, 'USD')
        return Account.objects.create(**kwargs)
    return make_account


@pytest.fixture
def create_transaction(db, create_account):
    def make_transaction(**kwargs):
        if 'source_id' not in kwargs:
            source = create_account(balance=Money(100, 'USD'))
            kwargs['source_id'] = source.id
        if 'target_id' not in kwargs:
            target = create_account()
            kwargs['target_id'] = target.id
        if 'amount' not in kwargs:
            kwargs['amount'] = 30
        return Transaction.create(**kwargs)

    return make_transaction
