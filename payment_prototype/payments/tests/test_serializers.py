import uuid

import pytest

from payments.serializers import TransactionSerializer


@pytest.mark.django_db
def test_transaction_serializer_create(mocker):
    mocked_account = mocker.patch('payments.serializers.Account')
    mocked_account.objects.get.return_value.user = 'some_user'
    mocked_transaction_create = mocker.patch('payments.serializers.Transaction.create')
    data = {
        'source_id': uuid.uuid4(),
        'target_id': uuid.uuid4(),
        'amount': 50,
        'comment': 'birthday present'
    }

    serializer = TransactionSerializer(data=data, context={'request': mocker.Mock(user='some_user')})
    assert serializer.is_valid()
    serializer.save()
    mocked_transaction_create.assert_called_once_with(**data)


@pytest.mark.django_db
def test_transaction_serializer_retrieve(create_transaction):
    transaction = create_transaction()
    serializer = TransactionSerializer(instance=transaction)
    assert all(
        (field in serializer.data
         for field in [
             'id', 'source_id', 'target_id', 'target_currency', 'amount',
             'amount_currency', 'commission_amount', 'commission_amount_currency',
             'comment', 'created_at'
         ])
    )
    assert serializer.data.get('target_currency') == 'USD'
