from decimal import Decimal

import pytest
from django.core.exceptions import ValidationError
from djmoney.money import Money
from payments.models import Account, Transaction


@pytest.fixture(autouse=True)
def mock_signals(mocker):
    mocker.patch('payments.signals.actual_create_user_accounts')


@pytest.mark.django_db
def test_account_create(create_user):
    account = Account.objects.create(
        user=create_user(),
        balance=Money(0, 'USD'),
    )

    assert Account.objects.count() == 1
    assert account.is_active is True


@pytest.mark.django_db
def test_transaction_the_same_account_create(create_account):
    source = target = create_account()
    with pytest.raises(ValidationError):
        Transaction.create(
            source_id=source.id,
            target_id=target.id,
            amount=30,
        )


@pytest.mark.django_db
def test_transaction_create_not_enough_money(create_account):
    source = create_account()
    target = create_account()
    with pytest.raises(ValidationError):
        Transaction.create(
            source_id=source.id,
            target_id=target.id,
            amount=30,
        )


@pytest.mark.django_db
def test_transaction_same_user_same_currency_create(mocker, create_account):
    mocker.patch('payments.models.settings.COMMISSION_AMOUNT_PERCENT', 10)
    source = create_account(balance=Money(100, 'USD'))
    target = create_account(user=source.user, balance=Money(0, 'USD'))

    transaction = Transaction.create(
        source_id=source.id,
        target_id=target.id,
        amount=30,
    )

    assert transaction.commission_amount == Money(0, 'USD')
    source.refresh_from_db()
    assert source.balance == Money(70, 'USD')
    target.refresh_from_db()
    assert target.balance == Money(30, 'USD')


@pytest.mark.django_db
@pytest.mark.parametrize(
    'source_currency,target_currency,source_result_balance,target_result_balance,exchange_rate', [
        ('USD', 'USD', 67, 30, Decimal('1')),  # without currency exchange
        ('USD', 'EUR', 67, 15, Decimal('0.50')),  # with currency exchange
    ])
def test_transaction_with_exchange_rate_different_users_different_currency_create(
        mocker, create_account, source_currency, target_currency,
        source_result_balance, target_result_balance, exchange_rate
):
    mocker.patch('payments.models.settings.COMMISSION_AMOUNT_PERCENT', 10)
    mocker.patch('djmoney.contrib.exchange.models.get_rate', return_value=exchange_rate)
    source = create_account(balance=Money(100, source_currency))
    target = create_account(balance=Money(0, target_currency))

    transaction = Transaction.create(
        source_id=source.id,
        target_id=target.id,
        amount=30,
    )

    assert transaction.commission_amount == Money(3, source_currency)
    source.refresh_from_db()
    assert source.balance == Money(source_result_balance, source_currency)
    target.refresh_from_db()
    assert target.balance == Money(target_result_balance, target_currency)


@pytest.mark.django_db
def test_transaction_deposit(mocker, create_account):
    # mocker.patch('payments.models.settings.COMMISSION_AMOUNT_PERCENT', 10)
    account = create_account()
    comment = 'some_comment'

    transaction = Transaction.deposit(
        target_id=account.id,
        amount=30,
        comment=comment
    )

    assert transaction.commission_amount == Money(0, 'USD')
    assert transaction.comment == comment
    account.refresh_from_db()
    assert account.balance == Money(30, 'USD')
