import pytest

from payments.models import Transaction


@pytest.mark.django_db
def test_create_user_accounts_signal(mocker, create_user):
    mocker.patch(
        'payments.signals.settings.DEFAULT_USER_ACCOUNTS',
        ((53, 'USD'), (0, 'EUR'))
    )

    user = create_user()

    assert user.accounts.count() == 2
    usd_account = user.accounts.get(balance_currency='USD')
    eur_account = user.accounts.get(balance_currency='EUR')
    assert usd_account.balance.amount == 53
    assert eur_account.balance.amount == 0
    assert Transaction.objects.count() == 1
    transaction = Transaction.objects.first()
    assert transaction.source is None
    assert transaction.target == usd_account
    assert transaction.amount.amount == 53
    assert transaction.amount_currency == 'USD'
