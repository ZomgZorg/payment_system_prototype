import pytest
from django.urls import reverse
from djmoney.money import Money


@pytest.fixture
def api_client():
    from rest_framework.test import APIClient
    return APIClient()


@pytest.mark.django_db
def test_unauthorized_list_transactions_request(api_client):
    url = reverse('transaction-list')
    response = api_client.get(url)
    assert response.status_code == 403


@pytest.mark.django_db
def test_user_can_see_only_their_transactions(mocker, api_client, create_user, create_account, create_transaction):
    mocker.patch('payments.signals.actual_create_user_accounts')
    user1 = create_user()
    user2 = create_user()
    account_of_user1 = create_account(user=user1, balance=Money(100, 'USD'))
    account1_of_user2 = create_account(user=user2, balance=Money(100, 'USD'))
    account2_of_user2 = create_account(user=user2, balance=Money(0, 'USD'))
    transaction1 = create_transaction(
        source_id=account1_of_user2.id,
        target_id=account2_of_user2.id,
        amount=30
    )
    transaction2 = create_transaction(
        source_id=account_of_user1.id,
        target_id=account2_of_user2.id,
        amount=40
    )
    api_client.force_authenticate(user=user1)
    url = reverse('transaction-list')
    response = api_client.get(url)
    assert response.status_code == 200
    result = response.json()['results']
    assert len(result) == 1
    assert result[0]['id'] == str(transaction2.id)

    api_client.force_authenticate(user=user2)
    response = api_client.get(url)
    assert response.status_code == 200
    result = response.json()['results']
    assert len(result) == 2
