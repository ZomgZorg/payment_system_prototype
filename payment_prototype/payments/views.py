# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django_filters import rest_framework as filters
from django.db.models import Q
from rest_framework import viewsets, mixins
from rest_framework.permissions import IsAuthenticated

from payments.models import Transaction, Account
from payments.serializers import TransactionSerializer, AccountSerializer


class TransactionFilterSet(filters.FilterSet):
    account_id = filters.CharFilter(method='account_id_filter')
    ordering = filters.OrderingFilter(
        fields=(
            ('amount', 'amount'),
            ('created_at', 'created_at'),
        ),
    )

    def account_id_filter(self, queryset, name, value):
        return queryset.filter(Q(target_id=value) | Q(source_id=value))

    class Meta:
        model = Transaction
        fields = ['source_id', 'target_id', 'amount_currency', 'created_at']


class TransactionAPIViewSet(mixins.CreateModelMixin, mixins.RetrieveModelMixin,
                            mixins.ListModelMixin, viewsets.GenericViewSet):
    model = Transaction
    queryset = Transaction.objects.select_related('source__user', 'target__user').all()
    serializer_class = TransactionSerializer
    permission_classes = [IsAuthenticated]
    filterset_class = TransactionFilterSet

    def get_queryset(self):
        qs = super().get_queryset()
        return qs.filter(Q(source__user=self.request.user) | Q(target__user=self.request.user))


class AccountAPIViewSet(mixins.DestroyModelMixin, mixins.CreateModelMixin, mixins.RetrieveModelMixin,
                        mixins.ListModelMixin, viewsets.GenericViewSet):
    model = Account
    queryset = Account.objects.select_related('user').all()
    serializer_class = AccountSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        queryset = super().get_queryset()
        if self.request.user.is_authenticated:
            return queryset.filter(user=self.request.user)
        else:
            queryset = queryset.none()
        return queryset
