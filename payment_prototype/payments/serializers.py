from django.utils.translation import ugettext_lazy as _
from django.core.exceptions import ValidationError as ModelValidationError

from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from payments.models import Transaction, Account


class TransactionSerializer(serializers.ModelSerializer):
    source_id = serializers.UUIDField()
    target_id = serializers.UUIDField()
    target_currency = serializers.SerializerMethodField()

    class Meta:
        model = Transaction
        fields = (
            'id',
            'source_id',
            'target_id',
            'target_currency',
            'amount',
            'amount_currency',
            'commission_amount',
            'commission_amount_currency',
            'comment',
            'created_at',
        )
        extra_kwargs = {
            'ammount_currency': {'read_only': True},
            'commission_amount': {'read_only': True},
            'commission_amount_currency': {'read_only': True},
        }

    def get_target_currency(self, instance):
        if instance.target:
            return instance.target.balance_currency
        else:
            return None

    def validate_source_id(self, value):
        try:
            source = Account.objects.get(id=value)
        except Account.DoesNotExist:
            raise ValidationError(_('Wrong source account specified.'))
        if source.user != self.context['request'].user:
            raise ValidationError(_('Wrong source account specified.'))
        return value

    def validate_target_id(self, value):
        if not Account.objects.filter(id=value).exists():
            raise ValidationError(_('Wrong target account specified.'))
        return value

    def create(self, validated_data):
        try:
            instance = Transaction.create(
                source_id=validated_data.get('source_id'),
                target_id=validated_data.get('target_id'),
                amount=validated_data.get('amount'),
                comment=validated_data.get('comment', ''),
            )
        except ModelValidationError as e:
            raise ValidationError(str(e))
        return instance


class AccountSerializer(serializers.ModelSerializer):
    user = serializers.HiddenField(
        default=serializers.CurrentUserDefault(),
    )
    balance_currency = serializers.CharField()  # to make it required

    class Meta:
        model = Account
        fields = (
            'id',
            'user',
            'balance',
            'balance_currency',
            'created_at',
            'updated_at',
            'is_active'
        )

    def validate(self, attrs):
        # hack to probide default value for this field
        # because of intercations with balance_currency
        attrs['balance'] = 0
        return attrs
