from django.conf import settings
from django.contrib.auth import get_user_model
from django.db.models.signals import post_save
from django.dispatch import receiver
from djmoney.money import Money

from payments.models import Account, Transaction


def actual_create_user_accounts(instance):
    for account_default_balance, account_currency in settings.DEFAULT_USER_ACCOUNTS:
        account = Account.objects.create(user=instance, balance=Money(0, account_currency))
        if account_default_balance:
            Transaction.deposit(
                target_id=account.id,
                amount=account_default_balance,
                comment='Signup gift'
            )


@receiver(post_save, sender=get_user_model())
def create_user_accounts(sender, instance, created, **kwargs):
    if created:
        actual_create_user_accounts(instance)
