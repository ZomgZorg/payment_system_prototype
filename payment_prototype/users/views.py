from django.contrib.auth import get_user_model
from rest_framework.generics import CreateAPIView

from users.permissions import IsNotAuthenticated
from users.serializers import SignupSerializer


class SignupAPIView(CreateAPIView):
    model = get_user_model()
    permission_classes = [IsNotAuthenticated]
    serializer_class = SignupSerializer
