from django.contrib.auth import get_user_model
from django.contrib.auth.password_validation import validate_password
from django.utils.translation import ugettext_lazy as _
from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from rest_framework.validators import UniqueValidator

UserModel = get_user_model()


class SignupSerializer(serializers.ModelSerializer):
    password_repeat = serializers.CharField(write_only=True)

    class Meta:
        model = UserModel
        fields = (
            'username',
            'email',
            'password',
            'password_repeat',
            'first_name',
            'last_name',
        )
        extra_kwargs = {
            'email': {
                'required': True,
                'validators': [UniqueValidator(queryset=UserModel.objects.all())]
            },
            'password': {'write_only': True}
        }

    def validate(self, attrs):

        if 'password' in attrs and 'password_repeat' in attrs:
            if attrs['password'] != attrs['password_repeat']:
                raise ValidationError(_('Пароли должны совпадать'))
            del attrs['password_repeat']

        if 'password' in attrs:
            validate_password(attrs['password'], user=self.instance)

        return attrs

    def create(self, validated_data):
        password = validated_data['password']
        del validated_data['password']

        instance = super().create(validated_data)
        instance.set_password(password)
        instance.save()

        return instance
