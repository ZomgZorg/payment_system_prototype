from django.conf.urls import url
from django.contrib import admin
from django.urls import include
from rest_framework.routers import DefaultRouter
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)

from users.views import SignupAPIView
from payments.views import AccountAPIViewSet, TransactionAPIViewSet

router = DefaultRouter(trailing_slash=False)

router.register(r'accounts', AccountAPIViewSet)
router.register(r'transactions', TransactionAPIViewSet)

extra_methods = [
    url(r'^signup$', SignupAPIView.as_view(), name='signup'),
    url(r'^token$', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    url(r'^token/refresh$', TokenRefreshView.as_view(), name='token_refresh'),
]

api_urls = router.urls + extra_methods

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^api/', include(api_urls)),
]
