FROM python:3.7-alpine
ENV PYTHONUNBUFFERED 1
RUN apk update && apk add --no-cache \
    bash \
    postgresql-dev \
    python3-dev \
    musl-dev \
    gcc
RUN pip install psycopg2
RUN mkdir /application
WORKDIR /application
COPY payment_prototype/requirements.txt /application/
RUN pip install -r /application/requirements.txt
COPY . /application/
RUN chmod +x /application/wait-for-it.sh
RUN chmod +x /application/entrypoint.sh
