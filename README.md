Payment system prototype

Installation

cd into the root folder with docker-compose.yml file and copy env file

    cp .env.example .env

you also need to prepopulate FIXER_ACCESS_KEY variable in .env

then execute

     docker-compose build && docker-compose up -d

Usage

postman_collection - payment_prototype.postman_collection.json
